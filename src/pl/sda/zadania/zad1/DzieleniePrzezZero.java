package pl.sda.zadania.zad1;

import java.util.Scanner;

public class DzieleniePrzezZero {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbe a: ");
        int a = scanner.nextInt();
        System.out.println("Podaj luczbe b: ");
        int b=scanner.nextInt();

        metoda1(a,b);


        System.out.println("Wynik dzielenia: "+a/b);


    }
    private static void metoda_a(int a,int b){
        if(b<0){
            System.out.println("B<0");
        }
    }

    private static void metoda1(int a, int b){
        try{
            if(b<0){
                throw new Exception("B<0");
            }
        }
        catch (Exception e){
            //co ma sie stac jesli blad wystapi
            System.out.println("Wystąpił błąd, treść błędu: "+e.getMessage());
        }
    }

    //metoda 2 bardziej high pro bo ludki same musza obsluzyc ten blad
    private static void metoda2(int a, int b) throws Exception{
        if(b<0){
            throw new Exception("B<0");
        }
    }
}